import Vue from "vue"
import Router from "vue-router"

import Home from "./components/Home"
import Portfolio from "./components/portfolio/Portfolio"
import ListStocks from "./components/stocks/ListStocks"

Vue.use(Router)

export default new Router({
    mode: "history",
    routes: [
        { path: '/', component: Home },
        { path: '/portfolio', component: Portfolio },
        { path: '/stocks', component: ListStocks }
    ]
})