export default {
    state: {
        funds: 10000,
        stocks: []
    },
    mutations: {
        butStock(state, {stockId, quantity, stockPrice}) {
            const record = state.stocks.find(item => item.id == stockId)
            if(record) {
                record.quantity += quantity
            } else {
                state.stocks.push({
                    id: stockId,
                    quantity: quantity
                })
            }
            state.funds -= stockPrice * quantity
        },
        sellStock(state, {stockId, quantity, stockPrice}) {
            const record = state.stocks.find(item => item.id == stockId)
            if(record.quantity > quantity) {
                record.quantity -= quantity
            } else { //remover um elemento do array
                state.stocks.splice(state.stocks.indexOf(record), 1)
            }
            state.funds += stockPrice * quantity
        }
    },
    actions: {
        sellStock({commit}, order) {
            commit('sellStock', order)
        }
    },
    getters: {
        stockPortfolio(state, getters) {
            return state.store.map(item => {
                const record = getters.stocks.find(item => item.id == stockId)
                return {
                    id: stockId,
                    quantity: stock.quantity,
                    name: record.name,
                    price: record.price
                }
            })
        },
        funds(state) {
            return state.funds
        }
    }
}